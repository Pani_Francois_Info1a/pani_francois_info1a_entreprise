"""
    Fichier : 2_Connectionbd.py
    Auteur : OM 2021.03.03 Connection par l'instanciation de la classe Toolsbd.

    On obtient un objet "objet_connectbd"
    Cela permet de se connecter à la base de donnée.
"""

from Enterprise.database.database_tools import Toolsbd

try:

    """
        Une connection à la BD simplement avec l'instanciation de la "CLASSE" Toolsbd()
        Un curseur va être nécessaire pour se déplacer dans la BD.
    """
    objet_connectbd = Toolsbd()
    connect_mabd = objet_connectbd.connect_database()
    curseur_mabd = connect_mabd.cursor()

    """
        Une seule requête pour montrer la récupération des données dans la BD en MySql.
        Il n'y a aucun contrôle, aucun test, sans traitements d'erreurs.
        Tous ces tests restent à découvrir dans le sujet suivant.
    """
    strsql_client_afficher = """SELECT id_client, Titre FROM t_client ORDER BY id_client ASC"""
    curseur_mabd.execute(strsql_client_afficher)
    data_client = curseur_mabd.fetchall()

    print("data_client ", data_client, " Type : ", type(data_client))

    curseur_mabd.close()
    connect_mabd.close()

except Exception as ErreurConnectionBD:
    print(f"Connection à la BD Impossible !"
          f"{ErreurConnectionBD.args[0]} , "
          f"{ErreurConnectionBD}")



