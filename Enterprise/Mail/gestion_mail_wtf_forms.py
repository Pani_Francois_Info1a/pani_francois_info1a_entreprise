"""
    Fichier : gestion_facture_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjoutermail(FlaskForm):
    """
        Dans le formulaire "client_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    mail_regexp = ""
    mail_wtf = StringField("Mail : ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(mail_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])


    submit = SubmitField("Enregistrer le mail")


class FormWTFUpdatemail(FlaskForm):
    """
        Dans le formulaire "client_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    mail_update_regexp = ""
    mail_update_wtf = StringField("Mail : ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(mail_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])

    submit = SubmitField("Modifier le mail")


class FormWTFDeletemail(FlaskForm):
    """
        Dans le formulaire "client_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    mail_delete_wtf = StringField("Effacer ce client")
    submit_btn_del = SubmitField("Effacer client")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
