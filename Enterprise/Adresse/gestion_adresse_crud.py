"""
    Fichier : gestion_sim_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les sim.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from Enterprise import obj_mon_application
from Enterprise.database.connect_db_context_manager import MaBaseDeDonnee
from Enterprise.erreurs.exceptions import *
from Enterprise.erreurs.msg_erreurs import *
from Enterprise.Adresse.gestion_adresse_wtf_forms import FormWTFAjouteradresse
from Enterprise.Adresse.gestion_adresse_wtf_forms import FormWTFDeleteadresse
from Enterprise.Adresse.gestion_adresse_wtf_forms import FormWTFUpdateadresse

from pani_francois_info1a_entreprise.Enterprise.erreurs.msg_erreurs import error_codes

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /adresse_afficher

    Test : ex : http://127.0.0.1:5005/adresse_afficher

    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_adresse_sel = 0 >> tous les adresse(s).
                id_adresse_sel = "n" affiche le genre dont l'id est "n"
"""


@obj_mon_application.route("/adresse_afficher/<string:order_by>/<int:id_adresse_sel>", methods=['GET', 'POST'])
def adresse_afficher(order_by, id_adresse_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion adresse ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionadresse {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_adresse_sel == 0:
                    strsql_adresse_afficher = """SELECT * FROM t_adresse ORDER BY id_adresse ASC"""
                    mc_afficher.execute(strsql_adresse_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_adresse"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_id_adresse_selected_dictionnaire = {"value_id_adresse_selected": id_adresse_sel}
                    strsql_adresse_afficher = """SELECT * FROM t_adresse WHERE id_adresse = %(value_id_adresse_selected)s"""

                    mc_afficher.execute(strsql_adresse_afficher, valeur_id_adresse_selected_dictionnaire)
                else:
                    strsql_adresse_afficher = """SELECT * FROM t_adresse ORDER BY id_adresse DESC"""

                    mc_afficher.execute(strsql_adresse_afficher)

                data_adresse = mc_afficher.fetchall()

                print("data_adresse ", data_adresse, " Type : ", type(data_adresse))

                # Différencier les messages si la table est vide.
                if not data_adresse and id_adresse_sel == 0:
                    flash("""La table "t_adresse" est vide. !!""", "warning")
                elif not data_adresse and id_adresse_sel > 0:
                    # Si l'utilisateur change l'id_adresse dans l'URL et que le genre n'existe pas,
                    flash(f"L'adresse demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_adresse" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données adresse affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. adresse_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} adresse_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")
        data_row = data_adresse[0]

    # Envoie la page "HTML" au serveur.
    return render_template("Adresse/adresse_afficher.html", data=data_adresse, dataRow=data_row)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /adresse_ajouter

    Test : ex : http://127.0.0.1:5005/sim_ajouter

    Paramètres : sans

    But : Ajouter un genre pour un film

    Remarque :  Dans le champ "name_adresse_html" du formulaire "sim/sim_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/adresse_ajouter", methods=['GET', 'POST'])
def adresse_ajouter_wtf():
    form = FormWTFAjouteradresse()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion adresse ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionadresse {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                rue_wtf = form.rue_wtf.data
                rue = rue_wtf.lower()

                numero_wtf = form.numero_wtf.data
                numero = numero_wtf.lower()

                npa_wtf = form.npa_wtf.data
                npa = npa_wtf.lower()

                ville_wtf = form.ville_wtf.data
                ville = ville_wtf.lower()

                pays_wtf = form.pays_wtf.data
                pays = pays_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_rue": rue, "value_numero": numero, "value_npa": npa, "value_ville": ville, "value_pays": pays}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_adresse = "INSERT INTO t_adresse (`id_adresse`, `rue`, `numero`, `npa`, `ville`, `pays`) VALUES (NULL, %(value_rue)s, %(value_numero)s, %(value_npa)s, %(value_ville)s, %(value_pays)s)"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_adresse, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('adresse_afficher', order_by='DESC', id_adresse_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_adresse_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_adresse_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_adresse_crud:
            code, msg = erreur_gest_adresse_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion adresse CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_adresse_crud.args[0]} , "
                  f"{erreur_gest_adresse_crud}", "danger")

    return render_template("Adresse/adresse_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /adresse_update

    Test : ex cliquer sur le menu "sim" puis cliquer sur le bouton "EDIT" d'un "genre"

    Paramètres : sans

    But : Editer(update) un genre qui a été sélectionné dans le formulaire "adresse_afficher.html"

    Remarque :  Dans le champ "nom_adresse_update_wtf" du formulaire "sim/adresse_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/adresse_update", methods=['GET', 'POST'])
def adresse_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_adresse"
    id_adresse_update = request.values['id_adresse_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateadresse()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "adresse_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            rue_update = form_update.rue_update_wtf.data
            rue_update = rue_update.lower()

            numero_update = form_update.numero_update_wtf.data
            numero_update = numero_update.lower()

            npa_update = form_update.npa_update_wtf.data
            npa_update = npa_update.lower()

            ville_update = form_update.ville_update_wtf.data
            ville_update = ville_update.lower()

            pays_update = form_update.pays_update_wtf.data
            pays_update = pays_update.lower()

            valeur_update_dictionnaire = {"value_id_adresse": id_adresse_update, "value_rue": rue_update, "value_numero": numero_update, "value_npa": npa_update, "value_ville": ville_update, "value_pays": pays_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_rue = "UPDATE t_adresse SET rue = %(value_rue)s, numero = %(value_numero)s, npa = %(value_npa)s, ville = %(value_ville)s, pays = %(value_pays)s WHERE id_adresse = %(value_id_adresse)s"
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_rue, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_adresse_update"
            return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=id_adresse_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_adresse" et "Modele_adresse" de la "t_adresse"
            str_sql_id_adresse = "SELECT * FROM t_adresse WHERE id_adresse = %(value_id_adresse)s"
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_adresse, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_adresse = mybd_curseur.fetchone()
            print("data_adresse ", data_adresse, " type ", type(data_adresse), " adresse ",
                  data_adresse["rue"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "adresse_update_wtf.html"
            form_update.rue_update_wtf.data = data_adresse["rue"]
            form_update.numero_update_wtf.data = data_adresse["numero"]
            form_update.npa_update_wtf.data = data_adresse["npa"]
            form_update.ville_update_wtf.data = data_adresse["ville"]
            form_update.pays_update_wtf.data = data_adresse["pays"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_adresse_crud:
        code, msg = erreur_gest_adresse_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_adresse_crud} ", "danger")
        flash(f"Erreur dans adresse_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_adresse_crud.args[0]} , "
              f"{erreur_gest_adresse_crud}", "danger")
        flash(f"__KeyError dans adresse_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Adresse/adresse_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /sim_delete

    Test : ex. cliquer sur le menu "sim" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "adresse_afficher.html"

    Remarque :  Dans le champ "nom_sim_delete_wtf" du formulaire "sim/sim_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/adresse_delete", methods=['GET', 'POST'])
def adresse_delete_wtf():
    data_client_attribue_adresse_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_adresse"
    id_adresse_delete = request.values['id_adresse_btn_delete_html']

    # Objet formulaire pour effacer le adresse sélectionné.
    form_delete = FormWTFDeleteadresse()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("adresse_afficher", order_by="ASC", id_adresse_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_client_attribue_adresse_delete = session['data_client_attribue_adresse_delete']
                print("data_client_attribue_adresse_delete ", data_client_attribue_adresse_delete)

                flash(f"Effacer le adresse de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_client_adresse = """DELETE FROM t_client_adresse WHERE fk_adresse = %(value_id_adresse)s"""
                str_sql_delete_id_adresse = """DELETE FROM t_adresse WHERE id_adresse = %(value_id_adresse)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_adresse_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_adresse_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_client_adresse, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_adresse, valeur_delete_dictionnaire)

                flash(f"adresse définitivement effacé !!", "success")
                print(f"adresse définitivement effacé !!")

                # afficher les données
                return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_delete}
            print(id_adresse_delete, type(id_adresse_delete))

            # Requête qui affiche tous les films qui ont le genre que l'utilisateur veut effacer
            str_sql_adresse_delete = """SELECT id_client_adresse, Titre, id_adresse, rue FROM t_client_adresse 
                                            INNER JOIN t_client ON t_client_adresse.fk_client = t_client.id_client
                                            INNER JOIN t_adresse ON t_client_adresse.fk_adresse = t_adresse.id_adresse
                                            WHERE fk_adresse = %(value_id_adresse)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_adresse_delete, valeur_select_dictionnaire)
            data_client_attribue_adresse_delete = mybd_curseur.fetchall()
            print("data_client_attribue_adresse_delete...", data_client_attribue_adresse_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "sim/sim_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_client_attribue_adresse_delete'] = data_client_attribue_adresse_delete

            # Opération sur la BD pour récupérer "ID_adresse" et "intitule_genre" de la "t_adresse"
            str_sql_id_adresse = "SELECT * FROM t_adresse WHERE id_adresse = %(value_id_adresse)s"

            mybd_curseur.execute(str_sql_id_adresse, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_adresse = mybd_curseur.fetchone()
            print("data_adresse ", data_adresse, " type ", type(data_adresse), " adresse ",
                  data_adresse["rue"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "sim_delete_wtf.html"
            form_delete.adresse_delete_wtf.data = data_adresse["rue"]

            # Le bouton pour l'action "DELETE" dans le form. "sim_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_adresse_crud:
        code, msg = erreur_gest_adresse_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_adresse_crud} ", "danger")

        flash(f"Erreur dans adresse_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_adresse_crud.args[0]} , "
              f"{erreur_gest_adresse_crud}", "danger")

        flash(f"__KeyError dans adresse_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Adresse/adresse_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_adresse_associes=data_client_attribue_adresse_delete)
