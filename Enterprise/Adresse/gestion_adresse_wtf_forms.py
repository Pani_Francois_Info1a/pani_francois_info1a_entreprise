"""
    Fichier : gestion_sim_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import *
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouteradresse(FlaskForm):
    """
        Dans le formulaire "sim_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    rue_regexp = "^[A-Z 0-9]+$"
    #rue_wtf = StringField("Tappe le modele du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(rue_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    rue_wtf = StringField("Rue : ")

    numero_regexp = "^[A-Z 0-9]+$"
    #numero_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(numero_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    numero_wtf = StringField("Numéro : ")

    npa_regexp = "^[A-Z 0-9]+$"
    # npa_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(npa_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    npa_wtf = StringField("Npa : ")

    ville_regexp = "^[A-Z 0-9]+$"
    # ville_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(ville_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    ville_wtf = StringField("Ville : ")

    pays_regexp = "^[A-Z 0-9]+$"
    # npa_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                               Regexp(pays_regexp,
    #                                                                      message="Pas de caractères "
    #                                                                              "spéciaux, "
    #                                                                              "d'espace à double, de double "
    #                                                                              "apostrophe, de double trait union")
    #                                                               ])
    pays_wtf = StringField("Pays : ")


    submit = SubmitField("Enregistrer l'adresse")


class FormWTFUpdateadresse(FlaskForm):
    """
        Dans le formulaire "sim_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    rue_update_regexp = "^[A-Z 0-9]+$"
    #nom_adresse_update_wtf = StringField("Tappe le modele du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(nom_adresse_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                     "spéciaux, "
    #                                                                                     "d'espace à double, de double "
    #                                                                                     "apostrophe, de double trait "
    #                                                                                     "union")
    #                                                                      ])
    rue_update_wtf = StringField("Rue : ")

    numero_update_regexp = "^[A-Z 0-9]+$"
    #Numero_de_serie_adresse_update_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(Numero_de_serie_adresse_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                    "spéciaux, "
    #                                                                                    "d'espace à double, de double "
    #                                                                                    "apostrophe, de double trait "
    #                                                                                     "union")
    #
    #                                                                     ])
    numero_update_wtf = StringField("Numéro : ")

    npa_update_regexp = "^[A-Z 0-9]+$"
    # Numero_de_serie_adresse_update_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(Numero_de_serie_adresse_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                    "spéciaux, "
    #                                                                                    "d'espace à double, de double "
    #                                                                                    "apostrophe, de double trait "
    #                                                                                     "union")
    #
    #                                                                     ])
    npa_update_wtf = StringField("Npa : ")

    ville_update_regexp = "^[A-Z 0-9]+$"
    # Numero_de_serie_adresse_update_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(Numero_de_serie_adresse_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                    "spéciaux, "
    #                                                                                    "d'espace à double, de double "
    #                                                                                    "apostrophe, de double trait "
    #                                                                                     "union")
    #
    #                                                                     ])
    ville_update_wtf = StringField("Ville : ")

    pays_update_regexp = "^[A-Z 0-9]+$"
    # Numero_de_serie_adresse_update_wtf = StringField("Tappe le numéro de série du adresse ", validators=[Length(min=2, max=20, message="min 2 max 20"),
    #                                                                      Regexp(Numero_de_serie_adresse_update_regexp,
    #                                                                             message="Pas de chiffres, de "
    #                                                                                     "caractères "
    #                                                                                    "spéciaux, "
    #                                                                                    "d'espace à double, de double "
    #                                                                                    "apostrophe, de double trait "
    #                                                                                     "union")
    #
    #                                                                     ])
    pays_update_wtf = StringField("Pays : ")
    submit = SubmitField("Update adresse")


class FormWTFDeleteadresse(FlaskForm):
    """
        Dans le formulaire "sim_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    adresse_delete_wtf = StringField("Effacer l'adresse")
    submit_btn_del = SubmitField("Effacer l'adresse")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
