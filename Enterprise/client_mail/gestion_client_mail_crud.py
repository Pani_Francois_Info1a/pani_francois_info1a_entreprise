"""
    Fichier : gestion_client_mail_crud.py
    Auteur : OM 2021.05.01
    Gestions des "routes" FLASK et des données pour l'association entre les films et les Client.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from Enterprise import obj_mon_application
from Enterprise.database.connect_db_context_manager import MaBaseDeDonnee
from Enterprise.erreurs.exceptions import *
from Enterprise.erreurs.msg_erreurs import *

"""
    Nom : client_mail_afficher
    Auteur : OM 2021.05.01
    Définition d'une "route" /client_mail_afficher
    
    But : Afficher les films avec les Client associés pour chaque film.
    
    Paramètres : id_client_sel = 0 >> tous les films.
                 id_client_sel = "n" affiche le film dont l'id est "n"
                 
"""


@obj_mon_application.route("/client_mail_afficher/<int:id_mail_sel>", methods=['GET', 'POST'])
def client_mail_afficher(id_mail_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as Exception_init_client_mail_afficher:
                code, msg = Exception_init_client_mail_afficher.args
                flash(f"{error_codes.get(code, msg)} ", "danger")
                flash(f"Exception _init_client_mail_afficher problème de connexion BD : {sys.exc_info()[0]} "
                      f"{Exception_init_client_mail_afficher.args[0]} , "
                      f"{Exception_init_client_mail_afficher}", "danger")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_client_mail_afficher_data = """SELECT id_mail, mail,
                                                            GROUP_CONCAT(Titre) as Clientmail FROM t_client_mail
                                                            RIGHT JOIN t_mail ON t_mail.id_mail = t_client_mail.fk_mail
                                                            LEFT JOIN t_client ON t_client.id_client = t_client_mail.fk_client
                                                            GROUP BY id_mail"""
                if id_mail_sel == 0:
                    # le paramètre 0 permet d'afficher tous les films
                    # Sinon le paramètre représente la valeur de l'id du film
                    mc_afficher.execute(strsql_client_mail_afficher_data)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    valeur_id_mail_selected_dictionnaire = {"value_id_mail_selected": id_mail_sel}
                    # En MySql l'instruction HAVING fonctionne comme un WHERE... mais doit être associée à un GROUP BY
                    # L'opérateur += permet de concaténer une nouvelle valeur à la valeur de gauche préalablement définie.
                    strsql_client_mail_afficher_data += """ HAVING id_mail= %(value_id_mail_selected)s"""

                    mc_afficher.execute(strsql_client_mail_afficher_data, valeur_id_mail_selected_dictionnaire)

                # Récupère les données de la requête.
                data_client_mail_afficher = mc_afficher.fetchall()
                print("data_client ", data_client_mail_afficher, " Type : ", type(data_client_mail_afficher))

                # Différencier les messages.
                if not data_client_mail_afficher and id_mail_sel == 0:
                    flash("""La table "t_mail" est vide. !""", "warning")
                elif not data_client_mail_afficher and id_mail_sel > 0:
                    # Si l'utilisateur change l'id_film dans l'URL et qu'il ne correspond à aucun film
                    flash(f"L'mail {id_mail_sel} demandé n'existe pas !!", "warning")
                else:
                    flash(f"Données mail et Client affichés !!", "success")

        except Exception as Exception_client_mail_afficher:
            code, msg = Exception_client_mail_afficher.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception client_mail_afficher : {sys.exc_info()[0]} "
                  f"{Exception_client_mail_afficher.args[0]} , "
                  f"{Exception_client_mail_afficher}", "danger")
        data_row = data_client_mail_afficher
    # Envoie la page "HTML" au serveur.
    return render_template("client_mail/client_mail_afficher.html", data=data_client_mail_afficher, dataRow=data_row)


"""
    nom: edit_genre_film_selected
    On obtient un objet "objet_dumpbd"

    Récupère la liste de tous les Client de l'mail sélectionné par le bouton "MODIFIER" de "client_mail_afficher.html"
    
    Dans une liste déroulante particulière (tags-selector-tagselect), on voit :
    1) Tous les Client contenus dans la "t_genre".
    2) Les Client attribués au film selectionné.
    3) Les Client non-attribués au film sélectionné.

    On signale les erreurs importantes

"""


@obj_mon_application.route("/edit_client_mail_selected", methods=['GET', 'POST'])
def edit_client_mail_selected():
    if request.method == "GET":
        try:
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_client_afficher = """SELECT * FROM t_client"""
                mc_afficher.execute(strsql_client_afficher)
            data_client_all = mc_afficher.fetchall()
            print("dans edit_client_mail_selected ---> data_client_all", data_client_all)

            # Récupère la valeur de "id_film" du formulaire html "client_mail_afficher.html"
            # l'utilisateur clique sur le bouton "Modifier" et on récupère la valeur de "id_film"
            # grâce à la variable "id_film_genres_edit_html" dans le fichier "client_mail_afficher.html"
            # href="{{ url_for('edit_genre_film_selected', id_film_genres_edit_html=row.id_film) }}"
            id_client_mail_edit = request.values['id_client_mail_edit_html']

            # Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_client_mail_edit'] = id_client_mail_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_mail_selected_dictionnaire = {"value_id_mail_selected": id_client_mail_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la fonction genres_films_afficher_data
            # 1) Sélection du film choisi
            # 2) Sélection des Client "déjà" attribués pour le film.
            # 3) Sélection des Client "pas encore" attribués pour le film choisi.
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_client_mail_selected, data_client_mail_non_attribues, data_client_mail_attribues = \
                client_mail_afficher_data(valeur_id_mail_selected_dictionnaire)

            print(data_client_mail_selected)
            lst_data_mail_selected = [item['id_mail'] for item in data_client_mail_selected]
            print("lst_data_mail_selected  ", lst_data_mail_selected,
                  type(lst_data_mail_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les Client qui ne sont pas encore sélectionnés.
            lst_data_client_mail_non_attribues = [item['id_client'] for item in data_client_mail_non_attribues]
            session['session_lst_data_client_mail_non_attribues'] = lst_data_client_mail_non_attribues
            print("lst_data_client_mail_non_attribues  ", lst_data_client_mail_non_attribues,
                  type(lst_data_client_mail_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les Client qui sont déjà sélectionnés.
            lst_data_client_mail_old_attribues = [item['id_client'] for item in data_client_mail_attribues]
            session['session_lst_data_client_mail_old_attribues'] = lst_data_client_mail_old_attribues
            print("lst_data_client_mail_old_attribues  ", lst_data_client_mail_old_attribues,
                  type(lst_data_client_mail_old_attribues))

            print(" data data_client_mail_selected", data_client_mail_selected, "type ", type(data_client_mail_selected))
            print(" data data_client_mail_non_attribues ", data_client_mail_non_attribues, "type ",
                  type(data_client_mail_non_attribues))
            print(" data_client_mail_attribues ", data_client_mail_attribues, "type ",
                  type(data_client_mail_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_client_mail_non_attribues = [item['Titre'] for item in data_client_mail_non_attribues]
            print("lst_all_client gf_edit_client_mail_selected ", lst_data_client_mail_non_attribues,
                  type(lst_data_client_mail_non_attribues))

        except Exception as Exception_edit_client_mail_selected:
            code, msg = Exception_edit_client_mail_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception edit_client_mail_selected : {sys.exc_info()[0]} "
                  f"{Exception_edit_client_mail_selected.args[0]} , "
                  f"{Exception_edit_client_mail_selected}", "danger")

    return render_template("client_mail/client_mail_modifier_tags_dropbox.html",
                           data_client=data_client_all,
                           data_mail_selected=data_client_mail_selected,
                           data_mail_attribues=data_client_mail_attribues,
                           data_client_non_attribues=data_client_mail_non_attribues)



@obj_mon_application.route("/client_mail_ajouter", methods=['GET', 'POST'])
def client_mail_ajouter_wtf():
    form = FormWTFAjoutermail()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Client ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionclient {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                mail_wtf = form.mail_wtf.data
                mail = mail_wtf.lower()

                valeurs_insertion_dictionnaire = {"value_mail": mail}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_mail = """INSERT INTO t_mail (`id_mail`, `mail`) VALUES (NOT NULL, %(value_mail)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_mail, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('mail_afficher', order_by='DESC', id_mail_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_mail_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_mail_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_mail_crud:
            code, msg = erreur_gest_mail_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion Client CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_mail_crud.args[0]} , "
                  f"{erreur_gest_mail_crud}", "danger")

    return render_template("client_mail/client_mail_ajouter_wtf.html", form=form)



"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /client_delete

    Test : ex. cliquer sur le menu "Client" puis cliquer sur le bouton "DELETE" d'un "client"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "facture_afficher.html"

    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "Client/client_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/update_client_mail_selected", methods=['GET', 'POST'])
def update_client_mail_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_mail_selected = session['session_id_client_mail_edit']
            print("session['session_id_client_mail_edit'] ", session['session_id_client_mail_edit'])

            # Récupère la liste des Client qui ne sont pas associés au film sélectionné.
            old_lst_data_client_mail_non_attribues = session['session_lst_data_client_mail_non_attribues']
            print("old_lst_data_client_mail_non_attribues ", old_lst_data_client_mail_non_attribues)

            # Récupère la liste des Client qui sont associés au film sélectionné.
            old_lst_data_client_mail_attribues = session['session_lst_data_client_mail_old_attribues']
            print("old_lst_data_client_mail_old_attribues ", old_lst_data_client_mail_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme Client dans le composant "tags-selector-tagselect"
            # dans le fichier "genres_films_modifier_tags_dropbox.html"
            new_lst_str_client_mail = request.form.getlist('name_select_tags')
            print("new_lst_str_client_mail ", new_lst_str_client_mail)

            # OM 2021.05.02 Exemple : Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_client_mail_old = list(map(int, new_lst_str_client_mail))
            print("new_lst_client_mail ", new_lst_int_client_mail_old, "type new_lst_client_mail ",
                  type(new_lst_int_client_mail_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2021.05.02 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genre_film".
            lst_diff_client_delete_b = list(
                set(old_lst_data_client_mail_attribues) - set(new_lst_int_client_mail_old))
            print("lst_diff_client_delete_b ", lst_diff_client_delete_b)

            # Une liste de "id_genre" qui doivent être ajoutés à la "t_genre_film"
            lst_diff_client_insert_a = list(
                set(new_lst_int_client_mail_old) - set(old_lst_data_client_mail_attribues))
            print("lst_diff_client_insert_a ", lst_diff_client_insert_a)

            # SQL pour insérer une nouvelle association entre
            # "fk_film"/"id_film" et "fk_genre"/"id_genre" dans la "t_genre_film"
            strsql_insert_client_mail = """INSERT INTO t_client_mail (id_client_mail, fk_client, fk_mail)
                                                    VALUES (NULL, %(value_fk_client)s, %(value_fk_mail)s)"""

            # SQL pour effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_client_mail = """DELETE FROM t_client_mail WHERE fk_client = %(value_fk_client)s AND fk_mail = %(value_fk_mail)s"""

            with MaBaseDeDonnee() as mconn_bd:
                # Pour le film sélectionné, parcourir la liste des Client à INSÉRER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_client_ins in lst_diff_client_insert_a:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_ins" (l'id du genre dans la liste) associé à une variable.
                    valeurs_mail_sel_client_sel_dictionnaire = {"value_fk_mail": id_mail_selected,
                                                               "value_fk_client": id_client_ins}

                    mconn_bd.mabd_execute(strsql_insert_client_mail, valeurs_mail_sel_client_sel_dictionnaire)

                # Pour le film sélectionné, parcourir la liste des Client à EFFACER dans la "t_genre_film".
                # Si la liste est vide, la boucle n'est pas parcourue.
                for id_client_del in lst_diff_client_delete_b:
                    # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                    # et "id_genre_del" (l'id du genre dans la liste) associé à une variable.
                    valeurs_mail_sel_client_sel_dictionnaire = {"value_fk_mail": id_mail_selected,
                                                               "value_fk_client": id_client_del}

                    # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
                    # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
                    # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
                    # sera interprété, ainsi on fera automatiquement un commit
                    mconn_bd.mabd_execute(strsql_delete_client_mail, valeurs_mail_sel_client_sel_dictionnaire)

        except Exception as Exception_update_client_mail_selected:
            code, msg = Exception_update_client_mail_selected.args
            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Exception update_client_mail_selected : {sys.exc_info()[0]} "
                  f"{Exception_update_client_mail_selected.args[0]} , "
                  f"{Exception_update_client_mail_selected}", "danger")

    # Après cette mise à jour de la table intermédiaire "t_genre_film",
    # on affiche les films et le(urs) genre(s) associé(s).
    return redirect(url_for('client_mail_afficher', id_mail_sel=id_mail_selected))


"""
    nom: genres_films_afficher_data

    Récupère la liste de tous les Client du film sélectionné par le bouton "MODIFIER" de "client_mail_afficher.html"
    Nécessaire pour afficher tous les "TAGS" des Client, ainsi l'utilisateur voit les Client à disposition

    On signale les erreurs importantes
"""


def client_mail_afficher_data(valeur_id_mail_selected_dict):
    print("valeur_id_mail_selected_dict...", valeur_id_mail_selected_dict)
    try:

        strsql_mail_selected = """SELECT id_mail, mail, GROUP_CONCAT(id_client) as Clientmail FROM t_client_mail
                                        INNER JOIN t_mail ON t_mail.id_mail = t_client_mail.fk_mail
                                        INNER JOIN t_client ON t_client.id_client = t_client_mail.fk_client
                                        WHERE id_mail = %(value_id_mail_selected)s"""

        strsql_client_mail_non_attribues = """SELECT id_client, Titre FROM t_client WHERE id_client not in(SELECT id_client as id_clientmail FROM t_client_mail
                                                    INNER JOIN t_mail ON t_mail.id_mail = t_client_mail.fk_mail
                                                    INNER JOIN t_client ON t_client.id_client = t_client_mail.fk_client
                                                    WHERE id_mail = %(value_id_mail_selected)s)"""

        strsql_client_mail_attribues = """SELECT id_mail, id_client, Titre FROM t_client_mail
                                            INNER JOIN t_mail ON t_mail.id_mail = t_client_mail.fk_mail
                                            INNER JOIN t_client ON t_client.id_client = t_client_mail.fk_client
                                            WHERE id_mail = %(value_id_mail_selected)s"""

        # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
        with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
            # Envoi de la commande MySql
            mc_afficher.execute(strsql_client_mail_non_attribues, valeur_id_mail_selected_dict)
            # Récupère les données de la requête.
            data_client_mail_non_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("client_mail_afficher_data ----> data_client_mail_non_attribues ", data_client_mail_non_attribues,
                  " Type : ",
                  type(data_client_mail_non_attribues))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_mail_selected, valeur_id_mail_selected_dict)
            # Récupère les données de la requête.
            data_mail_selected = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_mail_selected  ", data_mail_selected, " Type : ", type(data_mail_selected))

            # Envoi de la commande MySql
            mc_afficher.execute(strsql_client_mail_attribues, valeur_id_mail_selected_dict)
            # Récupère les données de la requête.
            data_client_mail_attribues = mc_afficher.fetchall()
            # Affichage dans la console
            print("data_client_mail_attribues ", data_client_mail_attribues, " Type : ",
                  type(data_client_mail_attribues))

            # Retourne les données des "SELECT"
            return data_mail_selected, data_client_mail_non_attribues, data_client_mail_attribues
    except pymysql.Error as pymysql_erreur:
        code, msg = pymysql_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.Error Erreur dans client_mail_afficher_data : {sys.exc_info()[0]} "
              f"{pymysql_erreur.args[0]} , "
              f"{pymysql_erreur}", "danger")
    except Exception as exception_erreur:
        code, msg = exception_erreur.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"Exception Erreur dans client_mail_afficher_data : {sys.exc_info()[0]} "
              f"{exception_erreur.args[0]} , "
              f"{exception_erreur}", "danger")
    except pymysql.err.IntegrityError as IntegrityError_client_mail_afficher_data:
        code, msg = IntegrityError_client_mail_afficher_data.args
        flash(f"{error_codes.get(code, msg)} ", "danger")
        flash(f"pymysql.err.IntegrityError Erreur dans client_mail_afficher_data : {sys.exc_info()[0]} "
              f"{IntegrityError_client_mail_afficher_data.args[0]} , "
              f"{IntegrityError_client_mail_afficher_data}", "danger")

