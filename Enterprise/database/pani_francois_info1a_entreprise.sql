-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 17 Mai 2021 à 20:46
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pani_francois_info1a_entreprise`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE `t_adresse` (
  `id_adresse` int(20) NOT NULL,
  `rue` varchar(20) NOT NULL,
  `numero` text NOT NULL,
  `npa` text NOT NULL,
  `ville` varchar(10) NOT NULL,
  `pays` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_adresse`
--

INSERT INTO `t_adresse` (`id_adresse`, `rue`, `numero`, `npa`, `ville`, `pays`) VALUES
(1, 'Rue du Simplon', '14', '1337', 'Vallorbe', 'Suisse');

-- --------------------------------------------------------

--
-- Structure de la table `t_client`
--

CREATE TABLE `t_client` (
  `id_client` int(11) NOT NULL,
  `Titre` varchar(11) NOT NULL,
  `Prenom` varchar(11) DEFAULT NULL,
  `Nom` varchar(11) DEFAULT NULL,
  `Date_Naissance` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_client`
--

INSERT INTO `t_client` (`id_client`, `Titre`, `Prenom`, `Nom`, `Date_Naissance`) VALUES
(1, 'Monsieur', 'bite', 'con', '2021-04-28 08:19:16'),
(3, 'bidule', NULL, NULL, '2021-04-28 08:20:51'),
(4, 'monsieur', NULL, NULL, '2021-04-28 08:22:37'),
(5, 'bidulev', NULL, NULL, '2021-05-12 08:12:38'),
(7, 'halo', NULL, NULL, '2021-05-17 18:54:20');

-- --------------------------------------------------------

--
-- Structure de la table `t_client_adresse`
--

CREATE TABLE `t_client_adresse` (
  `id_client_adresse` int(11) NOT NULL,
  `fk_client` int(11) NOT NULL,
  `fk_adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_client_facture`
--

CREATE TABLE `t_client_facture` (
  `id_client_facture` int(11) NOT NULL,
  `fk_client` int(11) DEFAULT NULL,
  `fk_facture` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_facture`
--

CREATE TABLE `t_facture` (
  `id_facture` int(11) NOT NULL,
  `facture` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE `t_mail` (
  `id_mail` int(11) NOT NULL,
  `mail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_recu`
--

CREATE TABLE `t_recu` (
  `id_recu` int(11) NOT NULL,
  `recu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  ADD PRIMARY KEY (`id_adresse`);

--
-- Index pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD PRIMARY KEY (`id_client`);

--
-- Index pour la table `t_client_adresse`
--
ALTER TABLE `t_client_adresse`
  ADD PRIMARY KEY (`id_client_adresse`),
  ADD KEY `fk_client` (`fk_client`,`fk_adresse`),
  ADD KEY `fk_adresse` (`fk_adresse`);

--
-- Index pour la table `t_client_facture`
--
ALTER TABLE `t_client_facture`
  ADD PRIMARY KEY (`id_client_facture`),
  ADD KEY `fk_client` (`fk_client`,`fk_facture`),
  ADD KEY `fk_facture` (`fk_facture`);

--
-- Index pour la table `t_facture`
--
ALTER TABLE `t_facture`
  ADD PRIMARY KEY (`id_facture`);

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`id_mail`);

--
-- Index pour la table `t_recu`
--
ALTER TABLE `t_recu`
  ADD PRIMARY KEY (`id_recu`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_client`
--
ALTER TABLE `t_client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_client_adresse`
--
ALTER TABLE `t_client_adresse`
  MODIFY `id_client_adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_client_facture`
--
ALTER TABLE `t_client_facture`
  MODIFY `id_client_facture` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `id_mail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_recu`
--
ALTER TABLE `t_recu`
  MODIFY `id_recu` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_client_adresse`
--
ALTER TABLE `t_client_adresse`
  ADD CONSTRAINT `t_client_adresse_ibfk_1` FOREIGN KEY (`fk_client`) REFERENCES `t_client` (`id_client`),
  ADD CONSTRAINT `t_client_adresse_ibfk_2` FOREIGN KEY (`fk_adresse`) REFERENCES `t_adresse` (`id_adresse`);

--
-- Contraintes pour la table `t_client_facture`
--
ALTER TABLE `t_client_facture`
  ADD CONSTRAINT `t_client_facture_ibfk_1` FOREIGN KEY (`fk_client`) REFERENCES `t_client` (`id_client`),
  ADD CONSTRAINT `t_client_facture_ibfk_2` FOREIGN KEY (`fk_facture`) REFERENCES `t_facture` (`id_facture`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
