"""
    Fichier : gestion_facture_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les Client.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from Enterprise import obj_mon_application
from Enterprise.database.connect_db_context_manager import MaBaseDeDonnee
from Enterprise.erreurs.exceptions import *
from Enterprise.erreurs.msg_erreurs import *
from Enterprise.Client.gestion_client_wtf_forms import FormWTFAjouterclient
from Enterprise.Client.gestion_client_wtf_forms import FormWTFDeleteclient
from Enterprise.Client.gestion_client_wtf_forms import FormWTFUpdateclient

from pani_francois_info1a_entreprise.Enterprise.erreurs.msg_erreurs import error_codes

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /client_afficher
    
    Test : ex : http://127.0.0.1:5005/client_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_client_sel = 0 >> tous les Client.
                id_client_sel = "n" affiche le Client dont l'id est "n"
"""


@obj_mon_application.route("/client_afficher/<string:order_by>/<int:id_client_sel>", methods=['GET', 'POST'])
def client_afficher(order_by, id_client_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Client ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionclient {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_client_sel == 0:
                    strsql_client_afficher = """SELECT * FROM t_client ORDER BY id_client ASC"""
                    mc_afficher.execute(strsql_client_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_genre"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du genre sélectionné avec un nom de variable
                    valeur_id_client_selected_dictionnaire = {"value_id_client_selected": id_client_sel}
                    strsql_client_afficher = """SELECT * FROM t_client WHERE id_client = %(value_id_client_selected)s"""

                    mc_afficher.execute(strsql_client_afficher, valeur_id_client_selected_dictionnaire)
                else:
                    strsql_client_afficher = """SELECT * FROM t_client ORDER BY id_client DESC"""

                    mc_afficher.execute(strsql_client_afficher)

                data_client = mc_afficher.fetchall()

                print("data_client ", data_client, " Type : ", type(data_client))

                # Différencier les messages si la table est vide.
                if not data_client and id_client_sel == 0:
                    flash("""La table "Client" est vide. !!""", "warning")
                elif not data_client and id_client_sel > 0:
                    # Si l'utilisateur change l'id_client dans l'URL et que le Client n'existe pas,
                    flash(f"Le Client demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_client" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données Client affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.client_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} client_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")
        data_row = data_client[0]

    # Envoie la page "HTML" au serveur.
    return render_template("Client/client_afficher.html", data=data_client, dataRow=data_row)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /client_ajouter
    
    Test : ex : http://127.0.0.1:5005/client_ajouter
    
    Paramètres : sans
    
    But : Ajouter un Client pour un adresse
    
    Remarque :  Dans le champ "name_client_html" du formulaire "Client/client_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/client_ajouter", methods=['GET', 'POST'])
def client_ajouter_wtf():
    form = FormWTFAjouterclient()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion Client ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionclient {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():

                name_client_wtf = form.name_client_wtf.data
                name_client = name_client_wtf.lower()

                Prenom_wtf = form.Prenom_wtf.data
                Prenom = Prenom_wtf.lower()

                Nom_wtf = form.Nom_wtf.data
                Nom = Nom_wtf.lower()

                Date_Naissance = form.Date_Naissance_wtf.data



                valeurs_insertion_dictionnaire = {"value_name_client": name_client, "value_Prenom": Prenom, "value_Nom": Nom, "value_Date_Naissance": Date_Naissance}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_client = """INSERT INTO t_client (`id_client`, `Titre`, `Prenom`, `Nom`, `Date_Naissance`) VALUES (NULL, %(value_name_client)s, %(value_Prenom)s, %(value_Nom)s, %(value_Date_Naissance)s)"""
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_client, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('client_afficher', order_by='DESC', id_client_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_client_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_client_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_client_crud:
            code, msg = erreur_gest_client_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion Client CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_client_crud.args[0]} , "
                  f"{erreur_gest_client_crud}", "danger")

    return render_template("Client/client_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /genre_update
    
    Test : ex cliquer sur le menu "Client" puis cliquer sur le bouton "EDIT" d'un "genre"
    
    Paramètres : sans
    
    But : Editer(update) un genre qui a été sélectionné dans le formulaire "facture_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_update_wtf" du formulaire "Client/client_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/client_update", methods=['GET', 'POST'])
def client_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_client"
    id_client_update = request.values['id_client_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateclient()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "client_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.

            Titre_update = form_update.Titre_update_wtf.data
            Titre_update = Titre_update.lower()

            Prenom_update = form_update.Prenom_update_wtf.data
            Prenom_update = Prenom_update.lower()

            Nom_update = form_update.Nom_update_wtf.data
            Nom_update = Nom_update.lower()

            Date_Naissance_update = form_update.Date_Naissance_update_wtf.data

            valeur_update_dictionnaire = {"value_id_client": id_client_update, "value_Titre": Titre_update, "value_Prenom": Prenom_update, "value_Nom": Nom_update, "value_Date_Naissance": Date_Naissance_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_client = """UPDATE t_client SET id_client = %(value_id_client)s, Titre = %(value_Titre)s, Prenom = %(value_Prenom)s, Nom = %(value_Nom)s, Date_Naissance = %(value_Date_Naissance)s WHERE id_client = %(value_id_client)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_client, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('client_afficher', order_by="ASC", id_client_sel=id_client_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "Titre" de la "t_client"
            str_sql_id_client = "SELECT * FROM t_client WHERE id_client = %(value_id_client)s"
            valeur_select_dictionnaire = {"value_id_client": id_client_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_client, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_client = mybd_curseur.fetchone()

            print("data_nom_client ", data_nom_client, " type ", type(data_nom_client), " client ",
                  data_nom_client["Titre"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "client_update_wtf.html"
            form_update.Titre_update_wtf.data = data_nom_client["Titre"]
            form_update.Prenom_update_wtf.data = data_nom_client["Prenom"]
            form_update.Nom_update_wtf.data = data_nom_client["Nom"]
            form_update.Date_Naissance_update_wtf.data = data_nom_client["Date_Naissance"]
            

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans client_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans client_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_geste_client_crud:
        code, msg = erreur_geste_client_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_geste_client_crud} ", "danger")
        flash(f"Erreur dans client_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_geste_client_crud.args[0]} , "
              f"{erreur_geste_client_crud}", "danger")
        flash(f"__KeyError dans client_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Client/client_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /client_delete
    
    Test : ex. cliquer sur le menu "Client" puis cliquer sur le bouton "DELETE" d'un "client"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "facture_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "Client/client_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/client_delete", methods=['GET', 'POST'])
def client_delete_wtf():
    data_adresse_attribue_client_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_genre"
    id_client_delete = request.values['id_client_btn_delete_html']

    # Objet formulaire pour effacer le genre sélectionné.
    form_delete = FormWTFDeleteclient()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("client_afficher", order_by="ASC", id_client_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "Client/client_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_adresse_attribue_client_delete = session['data_adresse_attribue_client_delete']
                print("data_adresse_attribue_client_delete ", data_adresse_attribue_client_delete)

                flash(f"Effacer le client de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer genre" qui va irrémédiablement EFFACER le genre
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_client": id_client_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_client_adresse = """DELETE FROM t_client_adresse WHERE fk_client = %(value_id_client)s"""
                str_sql_delete_id_client = """DELETE FROM t_client WHERE id_client = %(value_id_client)s"""
                # Manière brutale d'effacer d'abord la "fk_genre", même si elle n'existe pas dans la "t_genre_film"
                # Ensuite on peut effacer le genre vu qu'il n'est plus "lié" (INNODB) dans la "t_genre_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_client_adresse, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_id_client, valeur_delete_dictionnaire)

                flash(f"Client définitivement effacé !!", "success")
                print(f"Client définitivement effacé !!")

                # afficher les données
                return redirect(url_for('client_afficher', order_by="ASC", id_client_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_client": id_client_delete}
            print(id_client_delete, type(id_client_delete))

            # Requête qui affiche tous les client_adresse qui ont le genre que l'utilisateur veut effacer
            str_sql_client_adresse_delete = """SELECT id_client_adresse, rue, id_client, Titre FROM t_client_adresse 
                                            INNER JOIN t_adresse ON t_client_adresse.fk_adresse = t_adresse.id_adresse
                                            INNER JOIN t_client ON t_client_adresse.fk_client = t_client.id_client
                                            WHERE fk_client = %(value_id_client)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_client_adresse_delete, valeur_select_dictionnaire)
            data_adresse_attribue_client_delete = mybd_curseur.fetchall()
            print("data_adresse_attribue_client_delete...", data_adresse_attribue_client_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "Client/client_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_adresse_attribue_client_delete'] = data_adresse_attribue_client_delete

            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_client = "SELECT id_client, Titre FROM t_client WHERE id_client = %(value_id_client)s"

            mybd_curseur.execute(str_sql_id_client, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom genre" pour l'action DELETE
            data_nom_client = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_client, " type ", type(data_nom_client), " client ",
                  data_nom_client["Titre"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "client_delete_wtf.html"
            form_delete.nom_client_delete_wtf.data = data_nom_client["Titre"]

            # Le bouton pour l'action "DELETE" dans le form. "client_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans client_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans client_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_client_crud:
        code, msg = erreur_gest_client_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_client_crud} ", "danger")

        flash(f"Erreur dans client_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_client_crud.args[0]} , "
              f"{erreur_gest_client_crud}", "danger")

        flash(f"__KeyError dans client_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("Client/client_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_adresse_associes=data_adresse_attribue_client_delete)
