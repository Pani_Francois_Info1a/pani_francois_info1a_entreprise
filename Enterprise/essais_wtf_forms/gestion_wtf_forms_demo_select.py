"""
    Fichier : gestion_facture_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les Client.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from Enterprise import obj_mon_application
from Enterprise.database.connect_db_context_manager import MaBaseDeDonnee
from Enterprise.erreurs.msg_erreurs import *
from Enterprise.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "Client" puis cliquer sur le bouton "DELETE" d'un "genre"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "facture_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "Client/client_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    client_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_client"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_client.data:

            if form_demo.submit_btn_ok_dplist_client.data:
                print("Client sélectionné : ",
                      form_demo.client_dropdown_wtf.data)
                client_selectionne = form_demo.client_dropdown_wtf.data
                form_demo.client_dropdown_wtf.choices = session['client_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_client_afficher = """SELECT id_client, Titre FROM t_client ORDER BY id_client ASC"""
                mc_afficher.execute(strsql_client_afficher)

            data_client = mc_afficher.fetchall()
            print("demo_select_wtf data_client ", data_client, " Type : ", type(data_client))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            client_val_list_dropdown = []
            for i in data_client:
                client_val_list_dropdown.append(i['Titre'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # genre_val_list_dropdown = [(i["id_genre"], i["intitule_genre"]) for i in data_client]

            print("client_val_list_dropdown ", client_val_list_dropdown)

            form_demo.client_dropdown_wtf.choices = client_val_list_dropdown
            session['client_val_list_dropdown'] = client_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.client_dropdown_wtf.data = "philosophique"
            client_selectionne = form_demo.client_dropdown_wtf.data
            print("client choisi dans la liste :", client_selectionne)
            session['client_selectionne_get'] = client_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           client_selectionne=client_selectionne)
