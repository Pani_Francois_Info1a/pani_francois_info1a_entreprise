Module 104
---


# Faire fonctionner se projet :

* Démarrer le serveur MySql "uWamp"
  
* Lancer PHPMyAdmin
  
* Importer la base de donnée pani_francois_info1a_entreprise dans PHPMyAdmins
  
* Créer le .env pour que vous puissiez vous connectez a votre base de donnée sur PHPMyAdmins
  
  *  # Serveur MySql
* HOST_MYSQL="localhost"
* USER_MYSQL="root"
* PASS_MYSQL="root"
* PORT_MYSQL=3306
* NAME_BD_MYSQL="pani_francois_info1a_entreprise"
* NAME_FILE_DUMP_SQL_BD="../database/pani_francois_info1a_entreprise.sql"

# FLask Microframework
* ADRESSE_SRV_FLASK="127.0.0.1"
* DEBUG_FLASK=true
* PORT_FLASK=5005
* SECRET_KEY_FLASK="magnifiquejourneedelalicorne___arc_en_ciel_le_BALROG_vous suivra"

# Lancer le serveur
* Lancer 1_run_server_flask.py

* Ensuite cliquer sur l'adresse du serveur local 127.0.0.1:5005 

  